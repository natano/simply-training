CREATE TEMPORARY TABLE demoscore (like score);
INSERT INTO demoscore SELECT * FROM score WHERE username = 'natano';
UPDATE demoscore SET username = 'demo';
INSERT INTO score TABLE demoscore ON CONFLICT (username, guid) DO NOTHING;
DROP TABLE demoscore;

INSERT INTO custom_meter (username, steps_hash, trail_hash, meter)
SELECT 'demo', steps_hash, trail_hash, meter FROM custom_meter WHERE username = 'natano'
ON CONFLICT DO NOTHING;
